package com.example.greenlogin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

public class Discount extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount);
        setTitle("คูปองส่วนลด");

        TabLayout tabLayout = findViewById(R.id.discountcoupon);
        tabLayout.setTabGravity(tabLayout.GRAVITY_FILL);
        PagerAdapter2 pagerAdapter2 = new PagerAdapter2(getSupportFragmentManager(),tabLayout.getTabCount());
        final ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(pagerAdapter2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    public void onClickchang(View view){
        ImageView imageView = (ImageView) findViewById(R.id.imageView4);
        Intent intent = new Intent(Discount.this,Chang.class);
        startActivity(intent);
    }


}
