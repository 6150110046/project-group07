package com.example.greenlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Chang extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chang);
        setTitle("แลกคูปอง");
    }
    public void onClickback(View view){
        Button btn_back = (Button)findViewById(R.id.back);
        Intent intent = new Intent(Chang.this,Discount.class);
        startActivity(intent);
    }
    public void onClickconfirm(View view){
        Button btn_back = (Button)findViewById(R.id.confirm);
        Intent intent = new Intent(Chang.this,Confirm.class);
        startActivity(intent);
    }
}