package com.example.greenlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ChangGift extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chang_gif);
        setTitle("ยืนยันการแลกของรางวัล");
    }
    public void onClickgif(View view){
        Button button = (Button) findViewById(R.id.congif);
        Intent intent = new Intent(ChangGift.this,ConfirmGif.class);
        startActivity(intent);
    }
}