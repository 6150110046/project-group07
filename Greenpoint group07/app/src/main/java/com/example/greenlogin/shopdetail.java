package com.example.greenlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class shopdetail extends AppCompatActivity {
    TextView getNameShop, getNameOwner, getPhoneNo,getaddress;
    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopdetail);
        setTitle("ข้อมูลร้านค้า");

        getNameShop = findViewById(R.id.getNameShop);
        getNameOwner = findViewById(R.id.getNameOwner);
        getPhoneNo = findViewById(R.id.getPhoneNo);
        getaddress = findViewById(R.id.getaddress);

        Intent i=this.getIntent();
        String id=i.getExtras().getString("getNameShop");
        String path=i.getExtras().getString("getNameOwner");
        String desc=i.getExtras().getString("getPhoneNo");
        String address2=i.getExtras().getString("getAddShop");
        getNameShop.setText(id);
        getNameOwner.setText(desc);
        getPhoneNo.setText(path);
        getaddress.setText(address2);
    }
}