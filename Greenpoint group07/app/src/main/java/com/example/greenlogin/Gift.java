package com.example.greenlogin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class Gift extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gif);
        setTitle("ของรางวัล");
    }
    public void onClickchanggif(View view){
        ImageView imageView = (ImageView) findViewById(R.id.gif);
        Intent intent = new Intent(Gift.this, ChangGift.class);
        startActivity(intent);
    }
    public void onClickchanggif1(View view){
        ImageView imageView = (ImageView) findViewById(R.id.gif1);
        Intent intent = new Intent(Gift.this, ChangGift.class);
        startActivity(intent);
    }
    public void onClickchanggif2(View view){
        ImageView imageView = (ImageView) findViewById(R.id.gif2);
        Intent intent = new Intent(Gift.this, ChangGift.class);
        startActivity(intent);
    }
}