package com.example.greenlogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer {

    @SerializedName("cust_id")
    @Expose
    private String custId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email_cs")
    @Expose
    private String emailCs;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("username")
    @Expose
    private String username;

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailCs() {
        return emailCs;
    }

    public void setEmailCs(String emailCs) {
        this.emailCs = emailCs;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
