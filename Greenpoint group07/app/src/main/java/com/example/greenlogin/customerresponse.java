package com.example.greenlogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class customerresponse {

    @SerializedName("cust_id")
    @Expose
    private String custId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email_cs")
    @Expose
    private String emailCs;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("username")
    @Expose
    private String username;

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailCs() {
        return emailCs;
    }

    public void setEmailCs(String emailCs) {
        this.emailCs = emailCs;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("customer")
    @Expose
    private List<Customer> customer = null;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<Customer> getCustomer() {
        return customer;
    }

    public void setCustomer(List<Customer> customer) {
        this.customer = customer;
    }

}

class shopresponse{
    @SerializedName("shop_id")
    @Expose
    private String shopId;
    @SerializedName("name_shop")
    @Expose
    private String nameShop;
    @SerializedName("name_owner")
    @Expose
    private String nameOwner;
    @SerializedName("add_shop")
    @Expose
    private String addShop;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("email_sh")
    @Expose
    private String emailSh;
    @SerializedName("username_sh")
    @Expose
    private String usernameSh;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getNameShop() {
        return nameShop;
    }

    public void setNameShop(String nameShop) {
        this.nameShop = nameShop;
    }

    public String getNameOwner() {
        return nameOwner;
    }

    public void setNameOwner(String nameOwner) {
        this.nameOwner = nameOwner;
    }

    public String getAddShop() {
        return addShop;
    }

    public void setAddShop(String addShop) {
        this.addShop = addShop;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmailSh() {
        return emailSh;
    }

    public void setEmailSh(String emailSh) {
        this.emailSh = emailSh;
    }

    public String getUsernameSh() {
        return usernameSh;
    }

    public void setUsernameSh(String usernameSh) {
        this.usernameSh = usernameSh;
    }
}

class login{
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
