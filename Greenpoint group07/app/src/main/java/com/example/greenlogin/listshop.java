package com.example.greenlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class listshop extends BaseAdapter {

    private Context context;

    private List<shopresponse> shopLists;

    private LayoutInflater layoutInflater;


    public listshop(Context applicationContext, List<shopresponse> shopLists) {
        context = applicationContext;
        this.shopLists = shopLists;

    }

    @Override
    public int getCount() {
        return shopLists.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (layoutInflater == null){
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null){
            view = layoutInflater.inflate(R.layout.activity_listshop,null);
        }

        TextView shopname2 = view.findViewById(R.id.name_shop);
        TextView add_shop2 = view.findViewById(R.id.add_shop);

        shopresponse getlistshop = shopLists.get(i);

        shopname2.setText(getlistshop.getNameShop());
        add_shop2.setText(getlistshop.getAddShop());

        return view;
    }
}