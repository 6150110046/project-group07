package com.example.greenlogin;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class editprofile extends AppCompatActivity {
    EditText name,phone,mail;
    Menu menu_edit;
    Systeminterface systeminterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("โปรไฟล์");

        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        mail = findViewById(R.id.mail);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Systeminterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Systeminterface systeminterface = retrofit.create(Systeminterface.class);
        Call<customerresponse> getcustomer = systeminterface.cust_profile("3");
        getcustomer.enqueue(new Callback<customerresponse>() {
            @Override
            public void onResponse(Call<customerresponse> call, Response<customerresponse> response) {
                Toast.makeText(getApplicationContext(),"โหลดข้อมูลสำเร็จ",Toast.LENGTH_LONG).show();
                name.setText(response.body().getCustomer().get(0).getName());
                phone.setText(response.body().getCustomer().get(0).getPhoneNo());
                mail.setText(response.body().getCustomer().get(0).getEmailCs());
            }

            @Override
            public void onFailure(Call<customerresponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(),""+t,Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editprofit,menu);
        menu_edit = menu;
        menu_edit.findItem(R.id.save).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.edit:
                setTitle("Edit Profile");
                menu_edit.findItem(R.id.edit).setVisible(false);
                menu_edit.findItem(R.id.save).setVisible(true);
                return true;
            case R.id.save:
                setTitle("Profile");
                menu_edit.findItem(R.id.edit).setVisible(true);
                menu_edit.findItem(R.id.save).setVisible(false);
                Toast.makeText(getApplicationContext(), "save surcess", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }





    }

