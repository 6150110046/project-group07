package com.example.greenlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Confirm extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        setTitle("ยืนยันการแลกคูปอง");
    }
    public void onClickconchang(View view){
        Button btn_con = (Button)findViewById(R.id.conchang);
        Intent intent = new Intent(Confirm.this,Discount.class);
        startActivity(intent);
    }
}