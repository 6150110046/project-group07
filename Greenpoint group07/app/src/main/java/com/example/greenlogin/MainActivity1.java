package com.example.greenlogin;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity1 extends AppCompatActivity  {
    BottomNavigationView bottomNavigation;
    private ActionBar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        setTitle("ร้านค้า");

        getSupportFragmentManager().beginTransaction().replace(R.id.container,new FragmentShop()).commit();

        toolbar = getSupportActionBar();
        bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_shop: toolbar.setTitle("ร้านค้า");
                        openFragment(FragmentShop.newInstance("Shop", ""));
                        return true;
                    case R.id.navigation_exchange:
                        toolbar.setTitle("แลกแต้ม");
                        openFragment(FragmentExchange.newInstance("Exchange", ""));
                        return true;
                    case R.id.navigation_settings:
                        toolbar.setTitle("ตั้งค่า");
                        openFragment(FragmentSettings.newInstance("Settings", ""));
                        return true;
                }
                return false;
            }
        });
    }
    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();


    }
}