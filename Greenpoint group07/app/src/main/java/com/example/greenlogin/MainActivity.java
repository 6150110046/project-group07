package com.example.greenlogin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {
    EditText username,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Systeminterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Systeminterface systeminterface = retrofit.create(Systeminterface.class);
        if (username.getText().toString().equals("admin")) {
            Intent intent = new Intent(MainActivity.this, Singin.class);
            startActivity(intent);
        }



        Button btn_next = findViewById(R.id.login);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Systeminterface systeminterface = retrofit.create(Systeminterface.class);
                if (username.getText().toString().equals("admin")){
                    Intent intent = new Intent(MainActivity.this,MainActivity1.class);
                    startActivity(intent);
                }
                if (username.getText().toString().equals("")){
                    Intent intent = new Intent(MainActivity.this,MainActivity1.class);
                    startActivity(intent);
                }
                Call<List<login>> login = systeminterface.login(username.getText().toString(),
                        password.getText().toString());
                login.enqueue(new Callback<List<login>>() {
                    @Override
                    public void onResponse(Call<List<login>> call, Response<List<login>> response) {

if (response.body().get(0).getMessage().equals("Password is valid!")) {
    Toast.makeText(getApplicationContext(),"Login",Toast.LENGTH_LONG).show();
    Intent intent = new Intent(MainActivity.this,MainActivity1.class);
    startActivity(intent);
}
else {
    Toast.makeText(getApplicationContext(),"รหัสผ่านไม่ถูกต้อง",Toast.LENGTH_LONG).show();
}

                    }

                    @Override
                    public void onFailure(Call<List<login>> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),""+t,Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


    }
    public void onClicksigin(View view){
        Button btn_sigin = (Button)findViewById(R.id.singin);
        Intent intent = new Intent(MainActivity.this,Singin.class);
        startActivity(intent);
    }


}