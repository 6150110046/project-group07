package com.example.greenlogin;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentExchange#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentExchange extends Fragment {

    String[] option = {"แลกของรางวัล" ,"แลกคูปอง"};
    int[] imgoption = {R.drawable.boxgif,R.drawable.ticketdis};
    int[] imgnext = {R.drawable.ic_baseline_arrow_forward_ios_24,R.drawable.ic_baseline_arrow_forward_ios_24};
    ListView listView;




    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentExchange() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentExchange.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentExchange newInstance(String param1, String param2) {
        FragmentExchange fragment = new FragmentExchange();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_exchange, container, false);

        listView = view.findViewById(R.id.listoption);


        Optioncustom optionadther = new Optioncustom(getContext(),option,imgoption,imgnext);
        listView.setAdapter(optionadther);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i==0){
                    startActivity(new Intent(getContext(), Gift.class));

                }
                if (i==1){
                    startActivity(new Intent(getContext(),Discount.class));

                }


            }
        });
        return view;


    }
}