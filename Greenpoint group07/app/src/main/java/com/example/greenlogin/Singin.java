package com.example.greenlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Singin extends AppCompatActivity {
    CheckBox checkBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singin);
        setTitle("ลงทะเบียนผู้ใช้งานใหม่");
        final EditText username,password,name,shopname,phonenumber,email,address;
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        name = findViewById(R.id.name);
        phonenumber = findViewById(R.id.phonenumber);
        email = findViewById(R.id.email);
        address = findViewById(R.id.address);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Systeminterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final Systeminterface systeminterface = retrofit.create(Systeminterface.class);

        Button register = findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<ResponseBody> Resgister = systeminterface.register(
                        name.getText().toString(),
                        email.getText().toString(),
                        phonenumber.getText().toString(),
                        username.getText().toString(),
                        password.getText().toString());
                Resgister.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Toast.makeText(getApplicationContext(),"Rigester",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Singin.this,MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });
        checkBox = findViewById(R.id.checkBox);
        checkBox.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (checkBox.isChecked()) {
                Intent intent4 = new Intent(getApplicationContext(), Conditions.class);
                startActivity(intent4);
            }
            else {

            }
        }
    });

        }

    public void onClicklogin(View view){
        Button btn_next = (Button)findViewById(R.id.login);
        Intent intent = new Intent(Singin.this,MainActivity.class);
        startActivity(intent);
    }



}