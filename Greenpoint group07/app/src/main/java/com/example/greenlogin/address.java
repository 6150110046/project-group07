package com.example.greenlogin;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class address extends AppCompatActivity {
    Menu menu_edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("ที่อยู่ของฉัน");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editprofit,menu);
        menu_edit = menu;
        menu_edit.findItem(R.id.save).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.edit:
                setTitle("Edit Address");
                menu_edit.findItem(R.id.edit).setVisible(false);
                menu_edit.findItem(R.id.save).setVisible(true);
                return true;
            case R.id.save:
                setTitle("Address");
                menu_edit.findItem(R.id.edit).setVisible(true);
                menu_edit.findItem(R.id.save).setVisible(false);
                Toast.makeText(getApplicationContext(), "save surcess", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}